package com.appsiom.filmfind.data

import com.appsiom.filmfind.video.VideoListing
import com.appsiom.filmfind.video.VideoSearchResult

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}

class VideoRepository(
    private val videoNetworkDataSource: VideoNetworkDataSource
) {

    suspend fun getVideoSearchResult(search: String = "Inception") : VideoSearchResult {
        return videoNetworkDataSource.getVideoSearchResult(search)
    }

    companion object {

        val searchResultList: MutableList<VideoListing> = ArrayList()
        val searchResultMap: MutableMap<String, VideoListing> = HashMap()

        public fun addVideoListing(videoListing: VideoListing) {
            searchResultList.add(videoListing)
            searchResultMap[videoListing.imdbID] = videoListing
        }

        fun getSampleData(): List<VideoListing> {
            return listOf(
                VideoListing(
                    "Inception",
                    "2010",
                    "tt1375666",
                    "movie",
                    "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg"
                ),
                VideoListing(
                    "Inception: The Cobol Job",
                    "2010",
                    "tt5295894",
                    "movie",
                    "https://m.media-amazon.com/images/M/MV5BMjE0NGIwM2EtZjQxZi00ZTE5LWExN2MtNDBlMjY1ZmZkYjU3XkEyXkFqcGdeQXVyNjMwNzk3Mjk@._V1_SX300.jpg"
                ),
                VideoListing(
                    "Inception: Jump Right Into the Action",
                    "2010",
                    "tt5295990",
                    "movie",
                    "https://m.media-amazon.com/images/M/MV5BZGFjOTRiYjgtYjEzMS00ZjQ2LTkzY2YtOGQ0NDI2NTVjOGFmXkEyXkFqcGdeQXVyNDQ5MDYzMTk@._V1_SX300.jpg"
                ),
                VideoListing(
                    "The Crack: Inception",
                    "2019",
                    "tt6793710",
                    "movie",
                    "https://m.media-amazon.com/images/M/MV5BZTU1M2U4OWUtZTQ5OS00OWM1LTljN2EtMWJmZDgxNzUwZGNkXkEyXkFqcGdeQXVyMTA0MjU0Ng@@._V1_SX300.jpg"
                ),
                VideoListing(
                    "Inception: Motion Comics",
                    "2010–",
                    "tt1790736",
                    "series",
                    "https://m.media-amazon.com/images/M/MV5BNGRkYzkzZmEtY2YwYi00ZTlmLTgyMTctODE0NTNhNTVkZGIxXkEyXkFqcGdeQXVyNjE4MDMwMjk@._V1_SX300.jpg"
                ),
                VideoListing(
                    "Inception",
                    "2014",
                    "tt7321322",
                    "movie",
                    "https://m.media-amazon.com/images/M/MV5BYWJmYWJmNWMtZTBmNy00M2MzLTg5ZWEtOGU5ZWRiYTE0ZjVmXkEyXkFqcGdeQXVyNzkyOTM2MjE@._V1_SX300.jpg"
                ),
                VideoListing(
                    "Inception: 4Movie Premiere Special",
                    "2010",
                    "tt1686778",
                    "movie",
                    "N/A"
                ),
                VideoListing(
                    "WWA: The Inception",
                    "2001",
                    "tt0311992",
                    "movie",
                    "https://m.media-amazon.com/images/M/MV5BNTEyNGJjMTMtZjZhZC00ODFkLWIyYzktN2JjMTcwMmY5MDJlXkEyXkFqcGdeQXVyNDkwMzY5NjQ@._V1_SX300.jpg"
                ),
                VideoListing(
                    "Cyberalien: Inception",
                    "2017",
                    "tt7926130",
                    "movie",
                    "N/A"
                ),
                VideoListing(
                    "Inception: In 60 Seconds",
                    "2013",
                    "tt3262402",
                    "movie",
                    "N/A"
                )
            )
        }
    }
}
