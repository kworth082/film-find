package com.appsiom.filmfind

import android.content.ClipData
import android.os.Bundle
import android.view.DragEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.appsiom.filmfind.data.VideoRepository
import com.appsiom.filmfind.databinding.FragmentVideoDetailBinding
import com.appsiom.filmfind.video.VideoListing
import com.google.android.material.appbar.CollapsingToolbarLayout

/**
 * A fragment representing a single video detail screen.
 * This fragment is either contained in a [VideoListFragment]
 * in two-pane mode (on larger screen devices) or self-contained
 * on handsets.
 */
class VideoDetailFragment : Fragment() {

    /**
     * The placeholder content this fragment is presenting.
     */
    private var videoListing: VideoListing? = null

    lateinit var videoTypeTextView: TextView
    private var toolbarLayout: CollapsingToolbarLayout? = null

    private var _binding: FragmentVideoDetailBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val dragListener = View.OnDragListener { v, event ->
        if (event.action == DragEvent.ACTION_DROP) {
            val clipDataItem: ClipData.Item = event.clipData.getItemAt(0)
            val dragData = clipDataItem.text
            videoListing = VideoRepository.searchResultMap[dragData]
            updateContent()
        }
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_VIDEO_ID)) {
                // Load the placeholder content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                videoListing = VideoRepository.searchResultMap[it.getString(ARG_VIDEO_ID)]
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentVideoDetailBinding.inflate(inflater, container, false)
        val rootView = binding.root

        toolbarLayout = binding.toolbarLayout
        videoTypeTextView = binding.videoType

        updateContent()
        rootView.setOnDragListener(dragListener)

        return rootView
    }

    private fun updateContent() {
        toolbarLayout?.title = videoListing?.title

        // Show the placeholder content as text in a TextView.
        videoListing?.let {
            videoTypeTextView.text = it.type
        }
    }

    companion object {
        /**
         * The fragment argument representing the video ID that this fragment
         * represents.
         */
        const val ARG_VIDEO_ID = "video_id"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}