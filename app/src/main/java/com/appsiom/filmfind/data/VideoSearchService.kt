package com.appsiom.filmfind.data

import com.appsiom.filmfind.video.VideoSearchResult
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface VideoSearchService : VideoApi {
    @GET("/")
    override suspend fun getVideoSearchResult(
        @Query("s") search: String,
        //options= "type", "y", "r", "page", "callback", "v" (see https://www.omdbapi.com/#parameters)
        @QueryMap options : Map<String, String>?
    ): VideoSearchResult
}
