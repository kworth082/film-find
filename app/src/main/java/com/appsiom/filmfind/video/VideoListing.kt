package com.appsiom.filmfind.video

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

//    {
//        "Title": "Inception",
//        "Year": "2010",
//        "imdbID": "tt1375666",
//        "Type": "movie",
//        "Poster": "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg"
//    }
@Parcelize
data class VideoListing(
    @SerializedName("Title") val title: String,
    @SerializedName("Year") val year: String,
    @SerializedName("imdbID") val imdbID: String,
    @SerializedName("Type") val type: String,
    @SerializedName("Poster") val poster: String,
) : Parcelable

//{
//  "Search": [
//    {
//      "Title": "Inception",
//      "Year": "2010",
//      "imdbID": "tt1375666",
//      "Type": "movie",
//      "Poster": "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg"
//    },
//    {
//      "Title": "Inception: The Cobol Job",
//      "Year": "2010",
//      "imdbID": "tt5295894",
//      "Type": "movie",
//      "Poster": "https://m.media-amazon.com/images/M/MV5BMjE0NGIwM2EtZjQxZi00ZTE5LWExN2MtNDBlMjY1ZmZkYjU3XkEyXkFqcGdeQXVyNjMwNzk3Mjk@._V1_SX300.jpg"
//    }
//  ],
//  "totalResults": "2",
//  "Response": "True"
//}
@Parcelize
data class VideoSearchResult(
    @SerializedName("Search") val listings: List<VideoListing>,
    @SerializedName("totalResults") val totalResults: String,
    @SerializedName("Response") val response: String,
) : Parcelable
