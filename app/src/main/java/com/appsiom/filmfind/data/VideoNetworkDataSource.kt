package com.appsiom.filmfind.data

import com.appsiom.filmfind.video.VideoSearchResult

class VideoNetworkDataSource(
    private val videoApi: VideoApi
) {
    suspend fun getVideoSearchResult(search: String): VideoSearchResult {
        return videoApi.getVideoSearchResult(search)
    }
}

interface VideoApi {
    suspend fun getVideoSearchResult(
        search: String,
        options : Map<String, String>? = HashMap<String, String>()
    ): VideoSearchResult
}