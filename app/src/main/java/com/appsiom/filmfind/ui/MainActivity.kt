package com.appsiom.filmfind.ui

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.appsiom.filmfind.R.drawable.ic_launcher_background
import com.appsiom.filmfind.R.drawable.ic_launcher_foreground
import com.appsiom.filmfind.data.OmdbClient
import com.appsiom.filmfind.data.VideoNetworkDataSource
import com.appsiom.filmfind.data.VideoRepository
import com.appsiom.filmfind.data.VideoSearchService
import com.appsiom.filmfind.ui.theme.FilmFindTheme
import com.appsiom.filmfind.video.VideoListing
import kotlin.coroutines.cancellation.CancellationException

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val videoRepository = VideoRepository(
            VideoNetworkDataSource(
                OmdbClient.retrofit.create(
                    VideoSearchService::class.java
                )
            )
        )

        setContent {
            FilmFindTheme {
                // A surface container using the 'background' color from the theme
                Surface(
//                    shape = MaterialTheme.shapes.medium,
//                    elevation = 1.dp,
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var listings by rememberSaveable { mutableStateOf(listOf<VideoListing>()) }
                    VideoInfoList(videoListings = listings)
                    LaunchedEffect(Unit) {
                        try {
                            listings = videoRepository.getVideoSearchResult().listings
                        } catch (e : CancellationException) {
                            Log.w(this@MainActivity::class.simpleName, "Coroutine cancelled, perhaps due to configuration change? $e")
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun VideoInfoList(videoListings: List<VideoListing>) {
    LazyColumn {
        items(videoListings) { video ->
            VideoCard(video)
        }
    }
}

@Preview
@Composable
fun SampleVideoInfoList() {
    FilmFindTheme {
        VideoInfoList(
            VideoRepository.getSampleData()
        )
    }
}

@Composable
fun VideoCard(video: VideoListing) {
    Row(modifier = Modifier.padding(all = 8.dp)) {
        Box {
            Image(
                painter = painterResource(ic_launcher_background),
                contentDescription = "Default placeholder icon",
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape)
                    .border(1.5.dp, MaterialTheme.colors.secondary, CircleShape)
            )
            Image(
                painter = painterResource(ic_launcher_foreground),
                contentDescription = "Default placeholder icon",
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape)
            )
        }

        Spacer(modifier = Modifier.width(8.dp))

        // We keep track if the listing is expanded or not in this variable
        var isExpanded by rememberSaveable { mutableStateOf(false) }
        // surfaceColor will be updated gradually from one color to the other
        val surfaceColor: Color by animateColorAsState(
            if (isExpanded) MaterialTheme.colors.primary else MaterialTheme.colors.surface,
        )

        // We toggle the isExpanded variable when we click on this Column
        Column(
            modifier = Modifier
                .clickable {
                    isExpanded = !isExpanded
                }
                .fillMaxWidth()
        ) {
            Text(
                text = "You chose ${video.title}!",
                color = MaterialTheme.colors.secondaryVariant,
                style = MaterialTheme.typography.subtitle2
            )
            Spacer(modifier = Modifier.height(4.dp))

            Surface(
                shape = MaterialTheme.shapes.medium,
                elevation = 1.dp,
                // surfaceColor color will be changing gradually from primary to surface
                color = surfaceColor,
                // animateContentSize will change the Surface size gradually
                modifier = Modifier.animateContentSize().padding(1.dp)
            ) {
                Text(
                    text = "A ${video.type} from ${video.year}. \nThe URL for the poster is: ${video.poster}",
                    modifier = Modifier.padding(all = 4.dp),
                    // If the message is expanded, we display all its content
                    // otherwise we only display the first line
                    maxLines = if (isExpanded) Int.MAX_VALUE else 1,
                    style = MaterialTheme.typography.body2
                )
            }
        }
    }
}

@Preview(name = "Light Mode")
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun DefaultPreview() {
    FilmFindTheme {
        VideoCard(VideoListing( "Inception", "2010", "tt1375666", "movie", "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg"))
    }
}