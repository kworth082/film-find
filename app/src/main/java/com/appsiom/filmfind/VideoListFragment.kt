package com.appsiom.filmfind

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.appsiom.filmfind.data.VideoRepository
import com.appsiom.filmfind.databinding.FragmentVideoListBinding
import com.appsiom.filmfind.video.VideoListing
import com.appsiom.filmfind.video.VideoListingViewAdapter
import com.appsiom.filmfind.video.VideoSearchResult

/**
 * A Fragment representing a list of video search results. This fragment
 * has different presentations for handset and larger screen devices. On
 * handsets, the fragment presents a list of vidoes, which when touched,
 * lead to a {@link VideoDetailFragment} representing
 * video details. On larger screens, the Navigation controller presents the list of videos and
 * video details side-by-side using two vertical panes.
 */

class VideoListFragment : Fragment() {

    /**
     * Method to intercept global key events in the
     * video list fragment to trigger keyboard shortcuts
     * Currently provides a toast when Ctrl + Z and Ctrl + F
     * are triggered
     */
    private val unhandledKeyEventListenerCompat =
        ViewCompat.OnUnhandledKeyEventListenerCompat { v, event ->
            if (event.keyCode == KeyEvent.KEYCODE_Z && event.isCtrlPressed) {
                Toast.makeText(
                    v.context,
                    "Undo (Ctrl + Z) shortcut triggered",
                    Toast.LENGTH_LONG
                ).show()
                true
            } else if (event.keyCode == KeyEvent.KEYCODE_F && event.isCtrlPressed) {
                Toast.makeText(
                    v.context,
                    "Find (Ctrl + F) shortcut triggered",
                    Toast.LENGTH_LONG
                ).show()
                true
            }
            false
        }

    private var _binding: FragmentVideoListBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentVideoListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.addOnUnhandledKeyEventListener(view, unhandledKeyEventListenerCompat)

        val recyclerView: RecyclerView = binding.videoList

        // Leaving this not using view binding as it relies on if the view is visible the current
        // layout configuration (layout, layout-sw600dp)
        val videoDetailFragmentContainer: View? = view.findViewById(R.id.video_detail_nav_container)

        /** Click Listener to trigger navigation based on if you have
         * a single pane layout or two pane layout
         */
        val onClickListener = View.OnClickListener { videoListingView ->
            val videoListing = videoListingView.tag as VideoListing
            val bundle = Bundle()
            bundle.putString(
                VideoDetailFragment.ARG_VIDEO_ID,
                videoListing.imdbID
            )
            if (videoDetailFragmentContainer != null) {
                videoDetailFragmentContainer.findNavController()
                    .navigate(R.id.fragment_video_detail, bundle)
            } else {
                videoListingView.findNavController().navigate(R.id.show_video_detail, bundle)
            }
        }

        /**
         * Context click listener to handle Right click events
         * from mice and trackpad input to provide a more native
         * experience on larger screen devices
         */
        val onContextClickListener = View.OnContextClickListener { v ->
            val videoListing = v.tag as VideoListing
            Toast.makeText(
                v.context,
                "Context click of video " + videoListing.imdbID,
                Toast.LENGTH_LONG
            ).show()
            true
        }
        setupRecyclerView(recyclerView, onClickListener, onContextClickListener)

        fetchSearchResult(recyclerView)
    }

    private fun setupRecyclerView(
        recyclerView: RecyclerView,
        onClickListener: View.OnClickListener,
        onContextClickListener: View.OnContextClickListener
    ) {

        recyclerView.adapter = VideoListingViewAdapter(
            VideoRepository.searchResultList,
            onClickListener,
            onContextClickListener
        )
        VideoRepository.searchResultList.clear()
        VideoRepository.searchResultMap.clear()
        recyclerView.adapter?.let { it.notifyItemRangeRemoved(0, it.itemCount - 1) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun fetchSearchResult(recyclerView: RecyclerView) {
        throw NotImplementedError("This class not yet updated to use coroutines over RxJava")
//        VideoRepository.getVideoSearchResult()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(
//                { searchResult ->
//                    onFetchSearchResult(searchResult, recyclerView.adapter)
//                },
//                { throwable ->
//                    Log.e("HomeActivity", throwable.message ?: "onError")
//                    VideoRepository.addVideoListing(VideoListing( "onFailure", "Never", "0", "Unspecified", "Unspecified"))
//                    recyclerView.adapter?.let { it.notifyItemInserted(it.itemCount) }
//                }
//            )
    }

    private fun onFetchSearchResult(searchResult: VideoSearchResult, adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>?) {
        adapter?.let {
            val firstAvailablePosition = it.itemCount
            val listings = searchResult.listings
            for (listing in listings) {
                VideoRepository.addVideoListing(listing)
            }
            it.notifyItemRangeInserted(firstAvailablePosition, listings.size)
        }
    }
}
