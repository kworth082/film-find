package com.appsiom.filmfind.data

import com.appsiom.filmfind.BuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object OmdbClient {

    private const val BASE_URL = "https://www.omdbapi.com"

    val retrofit: Retrofit by lazy {
        val apiKeyInterceptor = Interceptor { chain ->
            var request = chain.request()
            // Be sure to add apiKey to local.properties file:
            // `apiKey="<key>"`
            // (properties added to BuildConfig by secrets-gradle-plugin)
            val url: HttpUrl = request.url().newBuilder().addQueryParameter("apikey", BuildConfig.apiKey).build()
            request = request.newBuilder().url(url).build()
            chain.proceed(request)
        }
        val okHttpClient = OkHttpClient()
            .newBuilder()
            .addInterceptor(apiKeyInterceptor)
            .build()
        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(okHttpClient)

        retrofitBuilder.build()
    }
}
